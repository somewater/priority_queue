package task1.modifyme

import gnu.trove.TIntIntHashMap

/**
 * Stores the distances (of type `W) to the labels `A`.
 **/
abstract class Distances[A, W] {
  /*
   * Retrieves the stored distance `W` at label `W` or Infinity[W] if not reached.
   */
  def distanceAt(a: A): W
  /*
   * Updates the distance for label `A` with the value `W`
   */
  def updated(label: A, value: W): Unit
}

object Distances {
  //def empty(implicit ev: Infinity[Int]): Distances[Int, Int] = new MapDistances(ev.infinity)

  // max required capacity for used graph
  def empty(implicit ev: Infinity[Int]): Distances[Int, Int] = new ArrayDistances(10000)
}

class ArrayDistances(capacity: Int)(implicit ev: Infinity[Int]) extends Distances[Int, Int] {
  private val maxValue: Int = ev.infinity

  private val array = new TIntIntHashMap(capacity)

  override def distanceAt(a: Int): Int = {
    val d = array.get(a)
    if (d == 0) maxValue else d
  }

  override def updated(label: Int, value: Int): Unit = {
    array.put(label, value)
    ()
  }
}

class MapDistances[A, W](capacity: Int)(implicit ev: Infinity[W]) extends Distances[A, W] {
  private val maxValue = ev.infinity
  var underlying = new java.util.HashMap[A, W]

  def distanceAt(a: A): W = {
    underlying.getOrDefault(a, maxValue)
  }

  def updated(label: A, value: W): Unit = {
    underlying.put(label, value)
    ()
  }
}
