package task1.modifyme

import gnu.trove.TIntHashSet

/*
 * A collection of visited labels `A`.
 */
abstract class Visited[A] {
  /*
   * Should return `true` if `this.updated` was never called with `a`.
   */
  def notVisited(a: A): Boolean
  /*
   * Remember that we visited label `a`
   */
  def updated(a: A): Unit
}

object Visited {
  //def empty(implicit ev: Infinity[Int]) = new SetVisited(new scala.collection.mutable.HashSet[Int])

  // max required capacity for used graph
  def empty = new ArrayVisited(10000)
}

class ArrayVisited(capacity: Int) extends Visited[Int] {
  private val array = new TIntHashSet(capacity)

  override def notVisited(a: Int): Boolean = !array.contains(a)
  override def updated(a: Int): Unit = {
    array.add(a)
    ()
  }
}

case class SetVisited(val underlying: scala.collection.mutable.Set[Int]) extends Visited[Int] {
  override def notVisited(a: Int) = !underlying.contains(a)
  override def updated(a: Int): Unit= {
    underlying += a
    ()
  }
}

