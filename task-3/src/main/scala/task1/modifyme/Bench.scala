package task1.modifyme

class Bench {
  private val times = collection.mutable.Map.empty[String, Long]

  def bench[A](name: String)(f: => A): A = {
    val ns = System.nanoTime()
    val a = f
    val dt = System.nanoTime() - ns
    times.update(name, dt + times.getOrElse(name, 0L))
    a
  }

  def result(): Unit = {
    val sum = times.values.sum
    times.toSeq.sortBy(_._2).reverse.foreach { case (k, v) =>
      printf("%20.20s %.2f %% (%d ms)\n", k, v.toDouble / sum * 100, v / 1000000)
    }
  }
}
