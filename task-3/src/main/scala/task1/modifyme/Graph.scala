package task1.modifyme

/**
 * Hint: you will need an efficient graph implementation 
 * - think about memory locality.
 * - change it sparingly as `task1.GenericGraph` and `task1.Dijkstra` which you cannot modify still needs to
 *   compile.
 * - your graph must correctly handle an unknown label
 */

// a graph with labels of type A and weights of type W
trait Graph[A, W] {
  def neighbours(label: A): Neighbours[A, W]
}

class IntGraph(maxLabel: Int,
               indixes: Array[Int],
               labels: Array[Int],
               weights: Array[Int]) extends Graph[Int, Int] {
  override def neighbours(label: Int): Neighbours[Int, Int] =
    if (label < 0 || label > maxLabel) EmptyNeighbours
    else new IntNeighbours(label)

  class IntNeighbours(label: Int) extends Neighbours[Int, Int] {
    val start = indixes(label)
    val end = indixes(label + 1)
    override def foreach(f: (Int, Int) => Unit): Unit = for (i <- start until end) f(labels(i), weights(i))
  }
}

object EmptyNeighbours extends Neighbours[Int, Int] {
  override def foreach(f: (Int, Int) => Unit): Unit = ()
}

object Graph {
  // return your instance of the graph reading from the generic graph
  def from(genericGraph: task1.GenericGraph[Int, Int]): Graph[Int, Int] = {
    val start = System.currentTimeMillis()
    val maxLabel = genericGraph.points.keysIterator.max
    val edges = genericGraph.points.valuesIterator.map(_.elements.size).sum
    val indixes: Array[Int] = Array.ofDim(maxLabel + 1 + 1) // plus one for end index of last point, plus one because we use label as index
    val labels: Array[Int] = Array.ofDim(edges)
    val weights: Array[Int] = Array.ofDim(edges)

    var pos = 0
    for ((point, neighbours) <- genericGraph.points.toSeq.sortBy(_._1)) {
      val start = pos
      for ((label, weight) <- neighbours.elements) {
        labels(pos) = label
        weights(pos) = weight
        pos += 1
      }
      indixes(point) = start
      indixes(point + 1) = pos
    }

    println(s"IntGraph created in: ${System.currentTimeMillis() - start} [ms]")
    new IntGraph(maxLabel, indixes, labels, weights)
  }
}

// neighbours of a label (of type A) of a graph with weights of type W
trait Neighbours[A, W] {
  // perform `f` on each neighbour (label, weight). The order of traversal doesn't matter as long as all
  // neighbours are reached.
  def foreach(f: (A, W) => Unit): Unit
}

