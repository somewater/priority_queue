package task2

import java.util.concurrent.atomic.AtomicIntegerArray

import scala.util.Random

class MultiQueue[A >: Null] private (
  // nQueues is guaranteed to be at least 2
  nQueues: Int
  // other arguments here if needed
  )(implicit ordering: Ordering[A]) {

  private val locks = new AtomicIntegerArray(nQueues)
  private val queues = Array.fill(nQueues)(Queue.empty[A])
  private val rnd = new Random()

  private def uniformRandom(): Int = rnd.nextInt(nQueues)
  private def tryToLock(i: Int): Boolean = locks.compareAndSet(i, 0, 1)
  private def unlock(i: Int): Unit = locks.set(i, 0)

  def isEmpty: Boolean = queues.forall(_.isEmpty)
  def size: Int        = queues.map(_.size).sum

  def insert(element: A): Unit = {
    require(element != null)
    var i = 0
    do {
      i = uniformRandom()
    } while (!tryToLock(i))
    queues(i).enqueue(element)
    unlock(i)
  }

  /*
   * Smallest elements (non-strictly) first.
   */
  def deleteMin(): Option[A] = {
    Option(deleteMin0())
  }

  private def deleteMin0(): A = {
    var i, j = 0
    do {
      i = uniformRandom()
      j = uniformRandom()
      val iValue = queues(i).entry
      val jValue = queues(j).entry
      if (iValue != null && jValue != null && ordering.gt(iValue, jValue)) {
        i = j
      }
    } while (!tryToLock(i))
    val e = queues(i).deleteMin()
    unlock(i)
    e
  }
}

object MultiQueue {
  // You can ignore the scaling factor and the actuall amount of processors just use the given nQueues.
  def empty[A >: Null](nQueues: Int)(implicit ordering: Ordering[A]): MultiQueue[A] = {
    new MultiQueue(math.max(2, nQueues))
  }
}
