package task2

import org.scalatest.{FreeSpec, Matchers}

import scala.util.Random

class MultiQueueTest extends FreeSpec with Matchers {

  class Inserter(mq: MultiQueue[Integer], elements: Array[Integer]) extends Runnable {
    override def run(): Unit = {
      elements.foreach(mq.insert)
    }
  }

  class Worker(mq: MultiQueue[Integer], n: Int) extends Runnable {
    var elements: Array[Integer] = null
    override def run(): Unit = {
      val builder = collection.mutable.ArrayBuffer.empty[Integer]
      while (builder.size < n) {
        mq.deleteMin() match {
          case Some(element) =>
            builder.append(element)
          case None =>
        }
      }
      elements = builder.toArray
    }
  }

  def generateUniqueRandom(n: Int): Array[Integer] = {
    val r = new Random()
    val inserted = collection.mutable.Set.empty[Integer]
    val builder = collection.mutable.ArrayBuilder.make[Integer]
    while (inserted.size < n) {
      val next = r.nextInt()
      if (!inserted.contains(next)) {
        builder += next
        inserted += next
      }
    }
    builder.result()
  }

  val Threads = 10
  val nQueues = Threads * 2

  "MultiQueue" - {
    "should handle empty elem" in {
      val mq = MultiQueue.empty[Integer](nQueues)
      mq.deleteMin() shouldBe None
      mq.size shouldBe 0
      mq.isEmpty shouldBe true
    }

    "should never lose elements in single thread scenario" in {
      val mq = MultiQueue.empty[Integer](nQueues)
      val elements = (1 to nQueues).toList
      elements.foreach(mq.insert(_))

      (1 to 1000).map(_ => mq.deleteMin()).collect { case Some(v) => v } .toSet shouldEqual elements.toSet
    }

    "should never lose elements in multi thread scenario" in {
      val mq = MultiQueue.empty[Integer](nQueues)
      val N = 100 // values per thread (per worker)

      val insertersAndValues = (1 to Threads).map { _ =>
        val elements = generateUniqueRandom(N)
        new Inserter(mq, elements) -> elements
      }

      val insertThreads = insertersAndValues.map { case (inserter, _) =>
        val t = new Thread(inserter)
        t.run()
        t
      }

      insertThreads.foreach(_.join())

      val workersAndThreads = (1 to Threads).map { _ =>
        val worker = new Worker(mq, N)
        val t = new Thread(worker)
        t.start()
        worker -> t
      }

      workersAndThreads.foreach(_._2.join())

      val insertedValues = insertersAndValues.flatMap(_._2)
      val fetchedValues = workersAndThreads.flatMap(_._1.elements)

      insertedValues.sorted shouldEqual fetchedValues.sorted
    }
  }
}
