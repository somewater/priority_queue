package review

import donotmodifyme.Scenario2._
import database._

import scala.util.Try

/*
 * We need to store `donotmodifyme.Scenario2.Datum` in an efficient manner. We found a very efficient database
 * implementation (donotmodifyme.Scenario2.database.*` now we need to provide a well behaved wrapper for a
 * java the libary `database`.
 */
object Scenario2 {
  class DatabaseUser(credentials: DatabaseCredentials) {
    def put(datum: Datum): Try[Unit] = Try {
      withConnection { c =>
        c.put(datum.key, datum.serializeContent)
      }
    }

    def getAll: Try[Seq[Datum]] = Try {
      withConnection { c =>
        c.keys.toSeq.map { key =>
          val bytes = c.fetch(key)
          Datum.deserialize(bytes) match {
            case Left(error) =>
              throw new RuntimeException(error)
            case Right(datum) =>
              datum
          }
        }
      }
    }

    private def withConnection[R](call: DatabaseConnection => R): R = {
      // TODO: or use connection pool for performance improvement
      val connection = DatabaseConnection.open(credentials)
      try {
        call(connection)
      } finally {
        connection.close()
      }
    }
  }
}
