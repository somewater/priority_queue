package review

import donotmodifyme.Scenario1._

import scala.annotation.tailrec
import scala.util.Try

object Scenario1 {
  /*
   * Given a `blackBoxPositiveInt` 
   *  - a black box procedure returning `Int` that's outside of our control (i.e an external library call
   * or a call to a service)
   *
   * blackBoxPositiveInt:
   *  - can side effect
   *  - is delivered by a third party, we don't know how it operates
   *
   * @param total non negative int
   *
   * @return the amount of calls to `blackBoxPositiveInt` needed, so that the sum of all returned values from
   * `blackBoxPositiveInt` would be equal to @input `total`
   */
  def process(total: Int): Try[Int] = Try {
    require(total >= 0)
    helper(total, 0)
  }

  @tailrec
  private def helper(total: Int, n: Int): Int = {
    if (total == 0) {
      n
    } else if (total < 0) {
      throw new RuntimeException("BlackBox returned too much")
    } else {
      val b = blackBoxPositiveInt
      require(b > 0, "Blackbox returned unexpected value")
      helper(total - b, n + 1)
    }
  }
}
